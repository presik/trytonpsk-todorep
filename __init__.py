# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool

from . import (
    configuration,
    invoice,
    ir,
    move,
    party,
    product,
    purchase,
    sale,
    statement,
    stock,
)


def register():
    Pool.register(
        product.Template,
        product.Product,
        purchase.Purchase,
        purchase.PurchaseLine,
        move.ShipmentIn,
        move.Move,
        sale.Sale,
        sale.SaleLine,
        move.UpdateUnitPriceStart,
        configuration.Configuration,
        statement.StatementLine,
        stock.ProductsByLocations,
        invoice.DiscountSpecialStart,
        ir.Cron,
        module='todorep', type_='model')
    Pool.register(
        move.UpdateUnitPrice,
        sale.SaleForceDraft,
        invoice.DiscountSpecial,
        module='todorep', type_='wizard')
    Pool.register(
        party.AgentDunningReport,
        stock.ShipmentTagsReport,
        module='todorep', type_='report')
