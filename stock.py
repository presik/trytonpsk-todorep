# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.report import Report
from trytond.transaction import Transaction


class ProductsByLocations(metaclass=PoolMeta):
    __name__ = 'stock.products_by_locations'
    position = fields.Function(fields.Many2One('product.position', 'Position'), 'get_position')

    def get_position(self, name=None):
        context = Transaction().context
        location = context.get('locations')[0]
        if self.product:
            for p in self.product.template.positions:
                if p.warehouse.id == location:
                    return p.position.id


class ShipmentTagsReport(Report):
    "Shipment Tags Report"
    __name__ = 'stock_shipment_barcode.shipment_tags_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        lines = []
        for s in records:
            for l in s.incoming_moves:
                for x in range(int(l.number_tags)):
                    lines.append(l)

        report_context['lines'] = lines
        return report_context
