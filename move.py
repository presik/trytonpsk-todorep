# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from trytond.model import ModelView, fields
from trytond.modules.product import price_digits
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.wizard import Button, StateTransition, StateView, Wizard

STATES = {
    'readonly': Eval('state').in_(['cancel', 'assigned', 'done']),
}


CODE_PRICE = {
    '1': 'N',
    '2': 'i',
    '3': 'D',
    '4': 'Y',
    '5': 'A',
    '6': 'M',
    '7': 'R',
    '8': 'T',
    '9': 'Z',
    '0': 'C',
}


class ShipmentIn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    @classmethod
    def receive(cls, shipments):
        super(ShipmentIn, cls).receive(shipments)
        for shipment in shipments:
            for move in shipment.incoming_moves:
                if move.description_product and move.description_product != move.product.description:
                    move.product.description = move.description_product
                    move.product.save()
                if move.sale_price_w_tax != move.product.list_price:
                    move.update_prices()


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'
    sale_price_w_tax = fields.Numeric('Sale Price Tax',
        digits=price_digits, states=STATES)
    final_cost_w_tax = fields.Function(fields.Numeric('Landed Cost Tax',
        digits=price_digits), 'get_final_cost_w_tax')
    description_product = fields.Text('Description Product', states=STATES)
    old_cost_price = fields.Function(fields.Numeric('Old Cost Price', digits=price_digits,
        states={'readonly': True}), 'get_last_cost')
    old_sale_price = fields.Numeric('Old Sale Price', digits=price_digits,
        states={'readonly': True})
    profit = fields.Function(fields.Float('Profit', digits=(16, 2),
        depends=['sale_price_w_tax']), 'on_change_with_profit')
    discount = fields.Float("Discount")
    number_tags = fields.Integer('Tags')

    @fields.depends('sale_price_w_tax', 'final_cost_w_tax')
    def on_change_with_profit(self, name=None):
        profit = 0
        if self.sale_price_w_tax and self.final_cost_w_tax and self.final_cost_w_tax > 0:
            profit = float((self.sale_price_w_tax / self.final_cost_w_tax) * 100)
        return profit

    @staticmethod
    def default_discount():
        return 0.0

    @fields.depends('description_product', 'product')
    def on_change_product(self, name=None):
        super(Move, self).on_change_product()
        self.description_product = self.product.description if self.product else ''

    @classmethod
    def search_rec_name(cls, name, clause):
        target = clause[2:][0][1:][:-1]
        # Get products by code
        target_words = target.split(' ')
        dom_search = []
        for tw in target_words:
            # Ignore words with less 3 letter because otherwise
            # can be huge search
            if len(tw) <= 1:
                continue
            clause = ['OR',
                ('product.template.name', 'ilike', '%' + tw + '%'),
                ('product.code', 'ilike', '%' + tw + '%'),
                ('product.description', 'like', '%' + tw + '%'),
                ('origin.sale.party.name', 'ilike', '%' + tw + '%', 'sale.line'),
            ]
            dom_search.append(clause)

        return dom_search

    @classmethod
    def create(cls, vlist):
        moves = super(Move, cls).create(vlist)

        for move in moves:
            sale_price_w_tax = getattr(move.product.template, 'sale_price_w_tax', None)
            if sale_price_w_tax:
                move.old_sale_price = move.product.template.sale_price_w_tax
            move.description_product = move.product.description
            description = move.product.description
            move.description = description.rstrip('\n') if description else None
            move.number_tags = int(move.quantity)
            move.save()
        return moves

    def get_last_cost(self, name=None):
        res = 0
        if self.product and self.origin and str(self.origin).startswith('purchase'):
            PurchaseLine = Pool().get('purchase.line')
            products = PurchaseLine.search_read([
                ('product', '=', self.product.id),
                ('purchase.purchase_date', '<', self.origin.purchase.purchase_date),
            ], fields_names=['unit_price'], order=[('create_date', 'DESC')], limit=1)
            if products:
                res = products[0]['unit_price']
        return res

    def get_final_cost_w_tax(self, name=None):
        Tax = Pool().get('account.tax')
        res = self.unit_price
        supplier_taxes = []

        date_tax = self.effective_date or Pool().get('ir.date').today()
        if self.product.supplier_taxes_used and self.unit_price:
            for tax in self.product.supplier_taxes_used:
                if tax.type == 'percentage' and tax.rate > 0:
                    supplier_taxes.append(tax)
            tax_list = Tax.compute(supplier_taxes,
                self.unit_price or Decimal('0.0'), 1, date_tax)
            res = sum([t['amount'] for t in tax_list], Decimal('0.0'))
            res = res + self.unit_price
            res = res.quantize(
                Decimal(1) / 10 ** self.__class__.final_cost_w_tax.digits[1],
            )
        return res

    @fields.depends('unit_price', 'discount')
    def on_change_discount(self):
        if self.discount and self.unit_price:
            self.unit_price = self.set_new_unit_price(self.discount)

    def set_new_unit_price(self, discount):
        unit_price = self.unit_price
        if self.origin:
            unit_price = self.origin.unit_price
        value = discount / 100 * -1
        new_unit_price = float(unit_price) - float(unit_price) * (value)
        return Decimal(new_unit_price).quantize(Decimal(100) ** -2)

    def update_prices(self):
        pool = Pool()
        Template = pool.get('product.template')
        template = Template(self.product.template.id)
        if self.shipment.__name__ != 'stock.shipment.in':
            return
        sale_price_taxed = self.sale_price_w_tax
        if sale_price_taxed:
            res = template.compute_reverse_list_price(sale_price_taxed)
            template.sale_price_w_tax = sale_price_taxed
            template.list_price = res
        self.product.last_cost = self.unit_price
        template.save()

    @classmethod
    def update_unit_price(cls, moves):
        for move in moves:
            if move.origin and move.origin.__name__ != 'purchase.line':
                unit_price = move._compute_unit_price(
                    unit_price=move.unit_price)
                if unit_price != move.unit_price:
                    move.unit_price = unit_price


class UpdateUnitPriceStart(ModelView):
    "Update Unit Price Wizard Start"
    __name__ = 'todorep.update_unit_price.start'
    discount = fields.Float('Discount', digits=(2, 2), required=True)


class UpdateUnitPrice(Wizard):
    "Update Unit Price Wizard"
    __name__ = 'todorep.update_unit_price'
    start = StateView('todorep.update_unit_price.start',
        'todorep.stock_update_unit_price_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        ShipmentIn = Pool().get('stock.shipment.in')
        Move = Pool().get('stock.move')
        shipments = ShipmentIn.browse(Transaction().context['active_ids'])
        for shipment in shipments:
            if shipment.state != 'draft':
                continue
            for move in shipment.incoming_moves:
                unit_price = move.set_new_unit_price(self.start.discount)
                Move.write([move], {
                    'discount': self.start.discount,
                    'unit_price': unit_price,
                })

        return 'end'
