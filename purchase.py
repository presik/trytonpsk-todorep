# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    @classmethod
    def proceed(cls, purchases):
        super(Purchase, cls).proceed(purchases)
        cls.update_last_cost(purchases)

    @classmethod
    def update_last_cost(cls, purchases):
        for purchase in purchases:
            for l in purchase.lines:
                l.product.last_cost = l.unit_price
                l.product.save()

    def get_move(self, move_type):
        move = super(Purchase, self).get_move(move_type)
        move.description = move.on_change_with_description()
        return move


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    @fields.depends('description')
    def on_change_product(self):
        super(PurchaseLine, self).on_change_product()
        if self.product and self.product.description:
            description = self.product.description
            self.description = description.replace('\n', ' ')
        else:
            self.description = None
