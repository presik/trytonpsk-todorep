import math
from datetime import date, datetime, timedelta
from decimal import Decimal

from trytond.i18n import gettext
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

from .exceptions import MissingInvoiceTaxError

_ZERO = Decimal('0.0')


def round_floor(x):
    return int(math.floor(x / 100.0)) * 100


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    payable_no_commission = fields.Function(fields.Integer('Payable'),
        'get_payable_no_commission')

    @classmethod
    def process(cls, sales):
        for sale in sales:
            cls.recheck_taxes(sale)
        super(Sale, cls).process(sales)

    @classmethod
    def cancel_or_delete_sales(cls):
        delta = datetime.now() - timedelta(days=1)
        sales = cls.search([
            ('state', 'in', ['draft', 'quotation']),
            ('create_date', '<=', delta),
            ('reservation', '=', True),
            ('moves', '=', None),
        ])
        to_delete = []
        to_cancel = []
        for sale in sales:
            if sale.state == 'draft' and not sale.number:
                to_delete.append(sale)
            else:
                to_cancel.append(sale)
        cls.delete(to_delete)
        cls.cancel(to_cancel)

    @classmethod
    def quote(cls, sales):
        super(Sale, cls).quote(sales)
        for sale in sales:
            if sale.agent and sale.agent.plan and sale.agent.plan.percentage:
                percentage = sale.agent.plan.percentage
                base_amount = sum(ln.base_price for ln in sale.lines)
                amount = sum(ln.unit_price for ln in sale.lines)
                if base_amount != amount:
                    total_dsc = 100 - (amount * 100 / base_amount)
                    if percentage > total_dsc:
                        percentage = Decimal(str(round(percentage - float(total_dsc), 2)))
                    elif percentage <= total_dsc:
                        percentage = 0
                cls.write([sale], {'commission': percentage})

    @classmethod
    def to_quote(cls, args):
        res = super(Sale, cls).to_quote(args)
        sale = cls(args['sale_id'])
        res['agent.'] = {'id': sale.agent.id, 'rec_name': sale.agent.rec_name} if sale.agent else None
        commission = sale.commission
        amount_commission = str(round(float(sale.untaxed_amount) * commission / 100, 2)) if commission else 0
        res['commission'] = commission
        res['amount_commission'] = amount_commission
        return res

    def _get_invoice_sale(self):
        "Return invoice"
        invoice = super(Sale, self)._get_invoice_sale()
        invoice.comment = self.comment
        return invoice

    @classmethod
    def workflow_to_end(cls, sales):
        sale = sales[0]
        cls.recheck_taxes(sale)
        super(Sale, cls).workflow_to_end(sales)

    @classmethod
    def recheck_taxes(cls, sale):
        for line in sale.lines:
            vat_required = None
            if line.product:
                for txr in line.product.account_category.customer_taxes_used:
                    if txr.type == 'percentage' and txr.rate > 0:
                        vat_required = txr.id
                        break
                if vat_required:
                    tax_obj = [t.id for t in line.taxes if t.rate and t.rate > 0]
                    if vat_required not in tax_obj:
                        raise MissingInvoiceTaxError(gettext('todorep.msg_missing_tax', product=line.product.rec_name))

    @classmethod
    def do_reconcile(cls, sales):
        super(Sale, cls).do_reconcile(sales)
        for sale in sales:
            if sale.agent and sale.commission:
                if sale.commission > _ZERO:
                    try:
                        cls.reconcile_sale_commission(sale)
                    except:
                        pass

    @classmethod
    def reconcile_sale_commission(cls, sale):
        MoveLine = Pool().get('account.move.line')
        accounts_rec = []
        lines_to_reconcile = []
        commission_payment = _ZERO
        for st_line in sale.payments:
            if st_line.statement.journal.kind == 'payment':
                commission_payment += st_line.amount
                for line in st_line.move.lines:
                    if line.account.id != st_line.account.id and not line.reconciliation:
                        lines_to_reconcile.append(line)
                        accounts_rec.append(line.account.id)
        if commission_payment != _ZERO:
            commissions_reconcile = cls.process_commission(sale, commission_payment, accounts_rec)
            if commissions_reconcile:
                lines_to_reconcile.extend(commissions_reconcile)
            if lines_to_reconcile:
                print(lines_to_reconcile, 'lines')
                MoveLine.reconcile(lines_to_reconcile)

    @classmethod
    def process_commission(cls, sale, commission_payment, accounts_rec):
        lines_to_reconcile = []
        invoice = sale.invoices[0]
        Invoice = Pool().get('account.invoice')
        Commission = Pool().get('commission')

        commissions = Commission.search([
            ('origin', '=', str(invoice)),
            # ('invoice_line.invoice.state', '!=', 'paid')

        ])
        if not commissions:
            return []
        commission = commissions[0]
        if commission_payment > 0:
            Commission.write(commissions, {'amount': commission_payment})
            Commission.invoice(commissions)
            commission.invoice_line.invoice.invoice_date = date.today()
            commission.invoice_line.invoice.payment_term = sale.payment_term.id
            commission.invoice_line.invoice.save()
            Commission.set_number_invoice(commission)
            Invoice.post([commission.invoice_line.invoice])
            for line in commission.invoice_line.invoice.move.lines:
                if line.account.type.payable and not line.reconciliation:
                    lines_to_reconcile.append(line)
        return lines_to_reconcile

    def get_payable_no_commission(self, name=None):
        amount = 0
        if self.agent and self.commission:
            value = float(self.untaxed_amount) * self.commission / 100
            amount = int(self.total_amount - Decimal(round_floor(value)))
        return amount

    @classmethod
    def get_data(cls, args, context=None):
        res = super(Sale, cls).get_data(args, context)
        sale_id = args['sale_id']
        sale, = cls.browse([sale_id])
        if sale.agent and sale.commission and sale.payable_no_commission:
            res['pos_notes'] = 'CXP > ' + str(sale.payable_no_commission)
        return res

    @classmethod
    def get_extras(cls, sale):
        res = super(Sale, cls).get_extras(sale)
        if sale.agent and sale.commission and sale.payable_no_commission:
            res['pos_notes'] = 'CXP > ' + str(sale.payable_no_commission)
        return res

    def get_move(self, shipment_type):
        move = super(Sale, self).get_move(shipment_type)
        move.description = move.on_change_with_description()
        return move


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    @classmethod
    def faster_set_quantity(cls, args, context=None):
        id = args['id']
        quantity = args['quantity']
        line, = cls.browse([id])
        values = {'quantity': quantity}
        cls.write([line], values)
        res = line._get_line_pos()
        return res


class SaleForceDraft(metaclass=PoolMeta):
    __name__ = 'sale_co.force_draft'

    def transition_force_draft(self):
        Commision = Pool().get('commission')
        Sale = Pool().get('sale.sale')
        ids = Transaction().context['active_ids']
        if not ids:
            return 'end'
        sales = Sale.browse(ids)
        for sale in sales:
            if sale.invoices:
                for inv in sale.invoices:
                    commisions_ = Commision.search([
                        ('origin', '=', 'account.invoice,' + str(inv.id)),
                    ])
                    Commision.delete(commisions_)
        super(SaleForceDraft, self).transition_force_draft()
        return 'end'
