# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from sql import Table
from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard, StateView, Button, StateReport, StateTransition
from trytond.transaction import Transaction

_ZERO = Decimal(0)


class ExpiredPortfolioReport(metaclass=PoolMeta):
    __name__ = 'invoice_report.expired_portfolio_report'

    @classmethod
    def get_notes(cls, invoice):
        if invoice.agent:
            return invoice.agent.rec_name


class DiscountSpecialStart(ModelView):
    'Discount Special Start'
    __name__ = 'todorep.discount_special.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    period = fields.Many2One('account.period', 'Period')
    discount = fields.Integer('Rate Discount', required=True)
    number = fields.Integer('Number', required=True)
    reference = fields.Char('Reference', required=True)
    max_amount = fields.Numeric('Max Amount', required=True)
    target_date = fields.Date('Target Date')
    validar = fields.Boolean('Validar')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class DiscountSpecial(Wizard):
    'Discount Special'
    __name__ = 'todorep.discount_special'
    start = StateView('todorep.discount_special.start',
        'todorep.discount_special_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Accept', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        if self.start.reference != '0987' or self.start.discount > 46:
            return 'end'
        Invoice = pool.get('account.invoice')
        Account = pool.get('account.account')
        Journal = pool.get('account.journal')
        Statement = pool.get('account.statement')
        StJournal = pool.get('account.statement.journal')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Reconciliation = pool.get('account.move.reconciliation')
        account_move = Table('account_move')
        account_invoice = Table('account_invoice')
        cursor = Transaction().connection.cursor()
        tax_sum = Decimal(0)
        cuenta_efectivo, = Account.search([
            ('code', '=', '110505'),
        ])
        journal = Journal.search([
            ('code', '=', 'EFECTIVO')
        ])
        st_journals = StJournal.search([
            ('kind', '=', 'electronic')
        ])

        if not journal:
            return 'end'

        journal = journal[0]

        # Fixme Ignore bank payments
        st_journal = None
        if st_journals:
            st_journal = st_journals[0]

        dom_inv = [
            ('party.name', '=', 'CLIENTE POS'),
            ('type', '=', 'out'),
            ('number_alternate', '=', None),
            ('invoice_type', '=', 'P'),
            # ('id', '=', 606903),
        ]
        if self.start.validar:
            dom_inv.append(
                ('state', 'in', ['paid']),
            )
        else:
            dom_inv.append(
                ('state', 'in', ['validated']),
            )

        print(' 1 ')
        if self.start.target_date:
            dom_inv.append(('move.date', '=', self.start.target_date))
            if st_journal:
                ignore_invoices = []
                statements = Statement.search([
                    ('journal', '=', st_journal.id),
                    ('date', '=', self.start.target_date),
                ])
                for s in statements:
                    for l in s.lines:
                        if not l.invoice:
                            continue
                        ignore_invoices.append(l.invoice.id)

                if ignore_invoices:
                    dom_inv.append(('id', 'not in', ignore_invoices))
        elif self.start.period:
            dom_inv.append(('move.period', '=', self.start.period.id))
        else:
            return 'end'

        if hasattr(Invoice, 'agent'):
            dom_inv.append(('agent', '=', None))

        invoices = Invoice.search(dom_inv)
        counter = 0

        def move_to_draft(move_id):
            # query = "UPDATE account_move SET state='draft' WHERE id=${move_id}"
            cursor.execute(*account_move.update(
                columns=[account_move.state],
                values=["draft"],
                where=account_move.id == move_id)
            )

        print(' 2 ')
        for invoice in invoices:
            if invoice.total_amount <= 0:
                continue

            sale = invoice.sales[0]
            if len(sale.payments) > 1:
                continue

            payment = sale.payments[0]

            to_fix = []
            counter += 1
            reconciliation_to_delete = []
            if tax_sum > self.start.max_amount or counter > self.start.number:
                break
            previous_invoice_amount = invoice.tax_amount
            cursor.execute(*account_invoice.update(
                columns=[account_invoice.state, account_invoice.invoice_report_cache],
                values=["validated", None],
                where=account_invoice.id == invoice.id)
            )

            retake_moves = []
            move_to_draft(payment.move.id)
            to_fix.append(payment.move)

            print( ' prueba 1 >> ', invoice.move.state)
            move_to_draft(invoice.move.id)
            xmove = Move(invoice.move.id)
            print( ' prueba 2 >> ', xmove.state)
            retake_moves.append(invoice.move.id)
            for mline in invoice.move.lines:
                if not mline.reconciliation:
                    continue
                for rl in mline.reconciliation.lines:
                    # to_fix.append(rl.move)
                    move_to_draft(rl.move.id)
                    if rl.move.id != invoice.move.id:
                        retake_moves.append(rl.move.id)

            to_delete = [m.id for m in to_fix]
            cmoves = Move.browse(to_delete)
            for m in cmoves:
                print(m.number, m.state, invoice.move.state)
                if invoice.move.state == 'posted':
                    continue
                for ml in m.lines:
                    if ml.reconciliation:
                        reconciliation_to_delete.append(ml.reconciliation)
                        break

            print('  x = ', reconciliation_to_delete)
            # print(' 3 > ', invoice.move.state)
            Reconciliation.delete(set(reconciliation_to_delete))
            Invoice.draft([invoice])
            invoice = Invoice(invoice.id)
            print(' 3 > ', invoice.number)
            if self.start.validar:
                continue

            Move.delete(cmoves)
            for line in invoice.lines:
                if line.type != 'line':
                    continue
                discount = self.start.discount / 100

                # discount = Decimal(value).quantize(Decimal(100) ** -2)
                unit_price = int((1 - discount) * float(line.unit_price))
                vals = {
                    'unit_price': unit_price,
                }
                if not line.taxes and line.product.customer_taxes_used:
                    vals['taxes'] = [('add', line.product.customer_taxes_used)]
                line.write([line], vals)
                # line.update_prices()
                line.save()
            Invoice.update_taxes(invoices)
            Invoice.write([invoice], {'number_alternate': 'X'})
            Invoice.post([invoice])
            to_reconcile = []
            amount = _ZERO

            for ml in invoice.move.lines:
                if ml.account.id == invoice.account.id:
                    to_reconcile.append(ml)
                    amount += ml.debit
            print(' 4 ------------- ')
            move, = Move.create([{
                'company': 1,
                'journal': journal.id,
                'period': invoice.move.period,
                'date': invoice.move.date,
                'state': 'draft',
            }])

            line1, = MoveLine.create([{
                'description': invoice.number,
                'account': invoice.account.id,
                'party': invoice.party.id,
                'debit': _ZERO,
                'credit': amount,
                'move': move.id,
            }])
            to_reconcile.append(line1)

            line2, = MoveLine.create([{
                'description': '',
                'account': cuenta_efectivo.id,
                'debit': amount,
                'credit': _ZERO,
                'move': move.id,
            }])
            print(' 5 ------------- ')
            MoveLine.reconcile(to_reconcile)
            print(' 6 ------------- ')
            Move.post([move])
            print(' 7 ------------- ')
            tax_sum += previous_invoice_amount - invoice.tax_amount

        return 'end'
