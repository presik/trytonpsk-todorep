# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from trytond.model import fields
from trytond.pool import Pool, PoolMeta


def round_num(value):
    return Decimal(str(round(value, 4)))


CODE_PRICE = {
    '1': 'N',
    '2': 'i',
    '3': 'D',
    '4': 'Y',
    '5': 'A',
    '6': 'M',
    '7': 'R',
    '8': 'T',
    '9': 'Z',
    '0': 'C',
}


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    encoded_cost_price = fields.Function(fields.Char('Encoded Cost Price'),
        'get_encoded_price')
    encoded_sale_price = fields.Function(fields.Char('Encoded Sale Price'),
        'get_encoded_price')

    def get_encoded_price(self, name=None, value=None):
        string = ''
        name = name[8:] if name else None
        if value:
            string = str(int(value))
        elif name == 'cost_price':
            if self.template.cost_price:
                string = str(int(self.get_final_cost_w_tax()))
        elif name == 'sale_price' and self.template.sale_price_w_tax:
            string = str(int(self.template.sale_price_w_tax))

        value = string
        for v in string:
            value = value.replace(v, CODE_PRICE[v])
        return value

    def get_final_cost_w_tax(self, name=None):
        Tax = Pool().get('account.tax')
        today = Pool().get('ir.date').today()
        Template = Pool().get('product.template')
        res = None
        supplier_taxes = []
        cost_price = self.last_cost or self.template.cost_price
        if self.supplier_taxes_used and cost_price:
            for tax in self.supplier_taxes_used:
                if tax.type == 'percentage' and tax.rate > 0:
                    supplier_taxes.append(tax)
            if supplier_taxes:
                tax_list = Tax.compute(supplier_taxes,
                    cost_price or Decimal('0.0'), 1, today)
                res = sum([t['amount'] for t in tax_list], Decimal('0.0'))
                res = res + cost_price
                res = res.quantize(
                    Decimal(1) / 10 ** Template.cost_price.digits[1])
        if not res:
            return cost_price
        return res

    # last_cost = fields.Numeric('Last Cost', digits=price_digits,
    #     help='Last purchase price')
